// Array contendo as informações dos integrantes
const equipe = [
    {
        "id": 1,
        "foto": "foto-alberto.jpg",
        "nome": "Alberto",
        "cargo": "Presidente",
        "idade": 20
    },
    {
        "id": 2,
        "foto": "foto-bruno.jpg",
        "nome": "Bruno",
        "cargo": "Diretor",
        "idade": 21
    },
    {
        "id": 3,
        "foto": "foto-helena.jpg",
        "nome": "Helena",
        "cargo": "Gerente",
        "idade": 22
    },
    {
        "id": 4,
        "foto": "foto-fernanda.jpg",
        "nome": "Fernanda",
        "cargo": "Desenvolvedor",
        "idade": 23
    },
    {
        "id": 5,
        "foto": "foto-diego.jpg",
        "nome": "Diego",
        "cargo": "Desenvolvedor",
        "idade": 24
    },
    {
        "id": 6,
        "foto": "foto-iris.jpg",
        "nome": "Iris",
        "cargo": "Desenvolvedor",
        "idade": 25
    },
    {
        "id": 7,
        "foto": "foto-hugo.jpg",
        "nome": "Hugo",
        "cargo": "Desenvolvedor",
        "idade": 26
    },
    {
        "id": 8,
        "foto": "foto-gustavo.jpg",
        "nome": "Gustavo",
        "cargo": "Desenvolvedor",
        "idade": 27
    },
    {
        "id": 9,
        "foto": "foto-gabriel.jpg",
        "nome": "Gabriel",
        "cargo": "Desenvolvedor",
        "idade": 28
    }
];
/**
 * Esta é uma função que aplica estilo ao elemento selecionado. 
 * Além disso, ela desfaz o efeito no penultimo elemento clicado
 * Por sua vez, ela recebe o identificador da posicao do array de pessoas
 * 
 * @example
 *   showPeopleInfo(1);
 *
 * @param   {Number} obrigatorio   Parametro obrigatório
 * @returns {Void}
 */
function showPeopleInfo(index) {
    peopleCardItem = document.getElementById('people-' + index);
    lastActiveElement = document.querySelector('.people-active');
    if (lastActiveElement != null) {
        lastActiveElement.classList.remove('people-active');
    }
    peopleCardItem.classList.add('people-active');

    changeProfileSection(index);
}
/**
 * Esta é uma função que altera o conteúdo da div.
 * que mostra as informações pessoais completas da pessoa
 * Por sua vez, ela recebe o identificador da posicao do array de pessoas
 *
 * @example
 *   changeProfileSection(1);
 *
 * @param   {Number} obrigatorio   Parametro obrigatório
 * @returns {Void}
 */
function changeProfileSection(index){
   people =  equipe[index];
   document.getElementById("profile-picture").src = 'assets/img/'+people.foto;
   document.getElementById("profile-name").innerHTML = people.nome;
   document.getElementById("profile-cargo").innerHTML = people.cargo;
   document.getElementById("profile-idade").innerHTML =  people.idade;

}
//DOM
document.addEventListener('DOMContentLoaded', function () {
    // Este loop preenche a view de acordo com os dados do array de pessoas
    equipe.forEach(function (equipe, index) {
        let component = '';
        component += '<div class="people" id="people-'+index+'" onclick="showPeopleInfo('+index+')">';
        component += '    <div class="picture-people">';
        component += '        <img src="assets/img/'+equipe.foto+'" alt="">';
        component += '        <div class="badge">'+(equipe.id)+'</div>  ';
        component += '    </div>';
        component += '    <div class="body-people">';
        component += '        <strong>'+equipe.nome+'</strong> <br />';
        component += '        <span class="text-grey">'+equipe.cargo+'</span>';
        component += '    </div>';
        component += '</div>';
        document.getElementById('team').innerHTML += component;
    });
    // Esta função foi chamada com o objetivo de delegar um valor default 
    // À tela de apresentação
    showPeopleInfo(0);
})
// Esta função é responsável por abrir o menu lateral
function openNav() {
    document.getElementById("mySidebar").style.width = "250px";
    document.querySelector(".container").style.marginLeft = "250px";
}
// Esta função é responsável por fechar o menu lateral
function closeNav() {
    document.getElementById("mySidebar").style.width = "0";
    document.querySelector(".container").style.marginLeft = "0";
}